#!/usr/bin/python3
# -*- coding: utf-8 -*-
from processors import Prototype
from telegram.ext import Updater
from telegram.ext import CommandHandler, CallbackQueryHandler
from telegram import KeyboardButton, ReplyKeyboardMarkup, ParseMode, InlineKeyboardButton, InlineKeyboardMarkup

import common

HELP_TEXT = """
Commands
  [/help]                   - show this description.        
  [/vmstat_help]            - show this description.        
  [/check]                  - bot alive check.        
  [/disk]                   - show disk check button.
  [/vmstat]                 - show vmstat(cpu, i/o, mem) button.
  [/server_logs]            - show server hycon logs button.
  [/oregon_disk]            - show oregon server df -h
  [/seoul_disk]             - show seoul server df -h
  [/london_disk]            - show london server df -h
  [/network1_disk]          - show network1 server df -h
  [/oregon_vm_status]       - show oregon server vmstat
  [/seoul_vm_status]        - show seoul server vmstat
  [/london_vm_status]       - show london server vmstat
  [/network1_vm_status]     - show network1 server vmstat
  [/oregon_logs]            - show oregon server logs
  [/seoul_logs]             - show seoul server logs
  [/london_logs]            - show london server logs
  [/network1_logs]          - show network1 server logs
"""


class Bot(Prototype):
    def __init__(self):
        self.name = self.__class__.__name__
        super().__init__(self.name)
        self.updater = Updater(token=self.config.TELEGRAM_API_TOKEN, use_context=True)
        self.dispatcher = self.updater.dispatcher
        self.add_handler()

    def add_handler(self):
        self.dispatcher.add_handler(CommandHandler('help', self._help))
        self.dispatcher.add_handler(CommandHandler('check', self._bot_check))
        self.dispatcher.add_handler(CommandHandler('vmstat_help', self._get_vmstat_description))
        self.dispatcher.add_handler(CommandHandler('oregon_disk', self._oregon_disk_status))
        self.dispatcher.add_handler(CommandHandler('seoul_disk', self._seoul_disk_status))
        self.dispatcher.add_handler(CommandHandler('london_disk', self._london_disk_status))
        self.dispatcher.add_handler(CommandHandler('network1_disk', self._network1_disk_status))
        self.dispatcher.add_handler(CommandHandler('disk', self._display_disk_check_menu))
        self.dispatcher.add_handler(CommandHandler('oregon_vm_status', self._oregon_vm_status))
        self.dispatcher.add_handler(CommandHandler('seoul_vm_status', self._seoul_vm_status))
        self.dispatcher.add_handler(CommandHandler('london_vm_status', self._london_vm_status))
        self.dispatcher.add_handler(CommandHandler('network1_vm_status', self._network1_vm_status))
        self.dispatcher.add_handler(CommandHandler('vmstat', self._display_vm_stat_check_menu))
        self.dispatcher.add_handler(CommandHandler('oregon_logs', self._oregon_logs))
        self.dispatcher.add_handler(CommandHandler('seoul_logs', self._seoul_logs))
        self.dispatcher.add_handler(CommandHandler('london_logs', self._london_logs))
        self.dispatcher.add_handler(CommandHandler('network1_logs', self._network1_logs))
        self.dispatcher.add_handler(CommandHandler('server_logs', self._display_get_logs_menu))
        self.dispatcher.add_handler(CallbackQueryHandler(self._button))
        self.dispatcher.add_error_handler(self.error)

    def error(self, update, context):
        """Log Errors caused by Updates."""
        self.logger.error('Update "%s" caused error "%s"', update, context.error)

    def run(self):
        self.logger.info('Telegram receive Bot start...')
        self.updater.start_polling()    # Bot start.
        self.updater.idle()             # 사용자가 Ctrl-C를 누르거나 프로세스가 SIGINT를 받을 때까지 봇을 실행한다.

    def stop(self):
        self.updater.stop()
        self.updater.is_idle = False

    def _help(self, update, context):
        context.bot.send_message(chat_id=update.message.chat_id, text=f'{HELP_TEXT}')

    def _get_vmstat_description(self, update, context):
        context.bot.send_photo(chat_id=update.message.chat_id, photo=open(f'{self.config.WORK_DIR}/vmstat_description.png', 'rb'))

    def _bot_check(self, update, context):
        self.logger.debug('Bot check call....')
        context.bot.send_message(chat_id=update.message.chat_id, text="Bot is alive...")

    def _disk_status(self, ipaddr):
        outputs = {}
        common.get_disk_space(ipaddr, output=outputs, logger=self.logger)
        return outputs

    def _oregon_disk_status(self, update, context):
        self.logger.debug('oregon_disk_status call...')
        outputs = self._disk_status(self.config.server_list['Oregon'])
        context.bot.send_message(chat_id=update.message.chat_id, text=outputs[self.config.server_list['Oregon']])

    def _seoul_disk_status(self, update, context):
        self.logger.debug('seoul_disk_status call...')
        outputs = self._disk_status(self.config.server_list['Seoul'])
        context.bot.send_message(chat_id=update.message.chat_id, text=outputs[self.config.server_list['Seoul']])

    def _london_disk_status(self, update, context):
        self.logger.debug('london_disk_status call...')
        outputs = self._disk_status(self.config.server_list['London'])
        context.bot.send_message(chat_id=update.message.chat_id, text=outputs[self.config.server_list['London']])

    def _network1_disk_status(self, update, context):
        self.logger.debug('network1_disk_status call...')
        outputs = self._disk_status(self.config.server_list['Network1'])
        context.bot.send_message(chat_id=update.message.chat_id, text=outputs[self.config.server_list['Network1']])

    def _display_disk_check_menu(self, update, context):
        oregon = InlineKeyboardButton(text='Oregon Disk', callback_data='Oregon_disk')
        seoul = InlineKeyboardButton(text='Seoul Disk', callback_data='Seoul_disk')
        london = InlineKeyboardButton(text='London Disk', callback_data='London_disk')
        network1 = InlineKeyboardButton(text='Nework1 Disk', callback_data='Network1_disk')
        menu_options = [[oregon, seoul], [london, network1]]
        reply_markup = InlineKeyboardMarkup(menu_options)
        update.message.reply_text('Select disk check server.', reply_markup=reply_markup)

    def _vm_status(self, ipaddr):
        outputs = {}
        common.get_vmstat_info(ipaddr, output=outputs, logger=self.logger)
        return outputs

    def _oregon_vm_status(self, update, context):
        self.logger.debug('oregon_vm_status call...')
        outputs = self._vm_status(self.config.server_list['Oregon'])
        context.bot.send_message(chat_id=update.message.chat_id, text=outputs[self.config.server_list['Oregon']])

    def _seoul_vm_status(self, update, context):
        self.logger.debug('seoul_vm_status call...')
        outputs = self._vm_status(self.config.server_list['Seoul'])
        context.bot.send_message(chat_id=update.message.chat_id, text=outputs[self.config.server_list['Seoul']])

    def _london_vm_status(self, update, context):
        self.logger.debug('london_vm_status call...')
        outputs = self._vm_status(self.config.server_list['London'])
        context.bot.send_message(chat_id=update.message.chat_id, text=outputs[self.config.server_list['London']])

    def _network1_vm_status(self, update, context):
        self.logger.debug('network1_vm_status call...')
        outputs = self._vm_status(self.config.server_list['Network1'])
        context.bot.send_message(chat_id=update.message.chat_id, text=outputs[self.config.server_list['Network1']])

    def _display_vm_stat_check_menu(self, update, context):
        oregon = InlineKeyboardButton(text='Oregon cpu,mem,io', callback_data='Oregon_vmstat')
        seoul = InlineKeyboardButton(text='Seoul cpu,mem,io', callback_data='Seoul_vmstat')
        london = InlineKeyboardButton(text='London cpu,mem,io', callback_data='London_vmstat')
        network1 = InlineKeyboardButton(text='Nework1 cpu,mem,io', callback_data='Network1_vmstat')
        menu_options = [[oregon, seoul], [london, network1]]
        reply_markup = InlineKeyboardMarkup(menu_options)
        update.message.reply_text('Select vmstat check server.', reply_markup=reply_markup)

    def _get_logs(self, area):
        outputs = {}
        common.get_logs(self.config.server_logs, area, output=outputs, logger=self.logger)
        return outputs

    def _oregon_logs(self, update, context):
        self.logger.debug('oregon_logs call...')
        outputs = self._get_logs('Oregon')
        context.bot.send_message(chat_id=update.message.chat_id, text=outputs[self.config.server_list['Oregon']])

    def _seoul_logs(self, update, context):
        self.logger.debug('seoul_log call...')
        outputs = self._get_logs('Seoul')
        context.bot.send_message(chat_id=update.message.chat_id, text=outputs[self.config.server_list['Seoul']])

    def _london_logs(self, update, context):
        self.logger.debug('london_log call...')
        outputs = self._get_logs('London')
        context.bot.send_message(chat_id=update.message.chat_id, text=outputs[self.config.server_list['London']])

    def _network1_logs(self, update, context):
        self.logger.debug('network1_log call...')
        outputs = self._get_logs('Network1')
        context.bot.send_message(chat_id=update.message.chat_id, text=outputs[self.config.server_list['Network1']])

    def _display_get_logs_menu(self, update, context):
        oregon = InlineKeyboardButton(text='Oregon logs', callback_data='Oregon_logs')
        seoul = InlineKeyboardButton(text='Seoul logs', callback_data='Seoul_logs')
        london = InlineKeyboardButton(text='London logs', callback_data='London_logs')
        network1 = InlineKeyboardButton(text='Nework1 logs', callback_data='Network1_logs')
        menu_options = [[oregon, seoul], [london, network1]]
        reply_markup = InlineKeyboardMarkup(menu_options)
        update.message.reply_text('Select get logs server.', reply_markup=reply_markup)

    def _button(self, update, context):
        query = update.callback_query
        self.logger.debug(query.data)
        outputs = {}
        if 'Oregon_disk' == query.data:
            outputs = self._disk_status(self.config.server_list['Oregon'])
        elif 'Seoul_disk' == query.data:
            outputs = self._disk_status(self.config.server_list['Seoul'])
        elif 'London_disk' == query.data:
            outputs = self._disk_status(self.config.server_list['London'])
        elif 'Network1_disk' == query.data:
            outputs = self._disk_status(self.config.server_list['Network1'])
        elif 'Oregon_vmstat' == query.data:
            outputs = self._vm_status(self.config.server_list['Oregon'])
        elif 'Seoul_vmstat' == query.data:
            outputs = self._vm_status(self.config.server_list['Seoul'])
        elif 'London_vmstat' == query.data:
            outputs = self._vm_status(self.config.server_list['London'])
        elif 'Network1_vmstat' == query.data:
            outputs = self._vm_status(self.config.server_list['Network1'])
        elif 'Oregon_logs' == query.data:
            outputs = self._get_logs('Oregon')
        elif 'Seoul_logs' == query.data:
            outputs = self._get_logs('Seoul')
        elif 'London_logs' == query.data:
            outputs = self._get_logs('London')
        elif 'Network1_logs' == query.data:
            outputs = self._get_logs('Network1')

        self.logger.debug(outputs)
        if outputs is not {}:
            query.edit_message_text(text=outputs[self.config.server_list[query.data.split('_')[0]]])
