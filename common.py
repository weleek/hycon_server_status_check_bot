# -*- coding: utf-8 -*-
import os
import logging
import colorlog
import time
import inspect
from datetime import datetime
from pathlib import Path
from pexpect import pxssh
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
import platform

SSH_KEY_FILE = f'{str(Path.home())}/.ssh/checker_id_rsa'
LOGFILE_PATH = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))


def os_check():
    os_type = 'linux'
    if platform.system() == 'Darwin':
        os_type = 'mac'
    else:
        os_type = 'linux'
    return os_type


def logger_init(module_name='', level='INFO', logger=None):
    logger = logging.getLogger(module_name) if logger is None else logger
    logger.setLevel(level)
    fmt = '%(log_color)s%(levelname)6s %(asctime)s %(name)16s(%(lineno)4s) %(threadName)16s - %(message)s'
    formatter = colorlog.ColoredFormatter(fmt)
    handler = colorlog.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    fh = logging.FileHandler(f'{LOGFILE_PATH}/error.log')
    fh.setLevel('ERROR')
    fh.setFormatter(logging.Formatter('%(levelname)6s %(asctime)s %(name)16s(%(lineno)4s) - %(message)s'))
    logger.addHandler(fh)

    return logger


def _get_client(ipaddr='', username='ubuntu', keyfile=SSH_KEY_FILE):
    ssh = pxssh.pxssh(timeout=60, encoding='utf-8')
    ssh.login(ipaddr, username, ssh_key=keyfile, sync_multiplier=4)
    return ssh


def get_disk_space(ipaddr, output={}, logger=None):
    try:
        ssh = _get_client(ipaddr)
        ssh.sendline(f'df -h')
        ssh.prompt()
        output[ipaddr] = ssh.before.strip()
        ssh.sendline(f'unset HISTFILE')
        ssh.logout()
        ssh.close()
    except Exception as e:
        if logger is not None:
            logger.exception(e)
        if 'ssh' in globals() and ssh is not None:
            ssh.logout()
            ssh.close()


def get_disk_usage_warning(ipaddr, output={}, logger=None):
    try:
        ssh = _get_client(ipaddr)
        ssh.sendline(f'df -h')
        ssh.prompt()
        disk_status = ssh.before.strip()
        for line in disk_status.split('\n'):
            if line.find('/dev/nvme0n1p1') != -1 and int(line.split(' ')[-2].replace('%', '')) >= 90:
                output[ipaddr] = line
        ssh.sendline(f'unset HISTFILE')
        ssh.logout()
        ssh.close()
    except Exception as e:
        if logger is not None:
            logger.exception(e)
        if 'ssh' in globals() and ssh is not None:
            ssh.logout()
            ssh.close()


def get_vmstat_info(ipaddr, output={}, logger=None):
    try:
        ssh = _get_client(ipaddr)
        ssh.sendline("vmstat")
        ssh.prompt()
        output[ipaddr] = ssh.before.strip()
        ssh.sendline(f'unset HISTFILE')
        ssh.logout()
        ssh.close()
    except Exception as e:
        if logger is not None:
            logger.exception(e)
        if 'ssh' in globals() and ssh is not None:
            ssh.logout()
            ssh.close()


def _find_reorganizing_log_lines(ssh, cmd):
    ssh.sendline(cmd)
    ssh.prompt()

    tmp = ssh.before.strip()
    log_lines = [v.replace('\r', '').strip() for i, v in enumerate(tmp.split('\n')) if i > 0 and v != '']
    result_lines = []
    if len(log_lines) <= 1:
        return None

    for line in log_lines.split('\n'):
        remove_blocks = int(line[line.find('removing'):line.find('blocks for')].replace('removing', '').strip())
        new_blocks = int(line[line.find('blocks for'):line.find('new blocks')].replace('blocks for', '').strip())
        if remove_blocks > 50 or new_blocks > 50:
            result_lines.append(line)

    return result_lines != [] and result_lines


def _find_warning_block_height_log_lines(ssh, cmd):
    ssh.sendline(cmd)
    ssh.prompt()

    tmp = ssh.before.strip()
    log_lines = [v.replace('\r', '').strip() for i, v in enumerate(tmp.split('\n')) if i > 0 and v != '']
    if len(log_lines) <= 1:
        return None
    result_lines = []
    result_lines.clear()
    for index, line in enumerate(log_lines):
        block_height = line[line.find(')\t('):line.find('Uncles')].replace(')\t(', '').split(',')[0]
        result_lines.append(block_height)
        if index > 0 and int(result_lines[index - 1]) > int(block_height):
            return log_lines
    return None


def _find_error_log_lines(ssh, cmd):
    ssh.sendline(cmd)
    ssh.prompt()

    log_lines = ssh.before.strip().replace('\r', '')
    if log_lines.split('\n')[1] == "0":
        return 0
    return int(log_lines.split('\n')[1].strip())


def get_logs(config, area, output={}, logger=None):
    try:
        settings = config[area]
        output[settings['IPADDR']] = {}
        try:
            ssh = _get_client(settings['IPADDR'])
        except Exception as e:
            logger.exception(e)
            return

        cmd = f'ls -l --color=never {settings["LOGPATH"]} | grep -v total | awk \'{{print $9}}\''
        ssh.sendline(cmd)
        ssh.prompt()

        ls = ssh.before.strip()
        folder_list = [v.strip() for i, v in enumerate(ls.split('\n')) if i > 0]
        folder_list.sort(reverse=True, key=lambda date: datetime.strptime(date, "%Y-%m-%d"))

        cmd = f'cat {settings["LOGPATH"]}/{folder_list[0]}/logFile.log | grep --color=never `date "+%Y-%m-%dT%H:%M:"`'
        ssh.sendline(cmd)
        ssh.prompt()
        output[settings['IPADDR']] = ssh.before.strip()

        ssh.sendline(f'unset HISTFILE')
        ssh.prompt()
        ssh.logout()
        ssh.close()
    except Exception as e:
        if logger is not None:
            logger.exception(e)
        if 'ssh' in globals() and ssh is not None:
            ssh.logout()
            ssh.close()


def get_log_by_keyword(config, area, output={}, logger=None):
    try:
        settings = config[area]
        output[area] = {}
        try:
            ssh = _get_client(settings['IPADDR'])
        except Exception as e:
            logger.exception(e)
            return

        cmd = f'ls -l --color=never {settings["LOGPATH"]} | grep -v total | awk \'{{print $9}}\''
        ssh.sendline(cmd)
        ssh.prompt()

        ls = ssh.before.strip()
        folder_list = [v.strip() for i, v in enumerate(ls.split('\n')) if i > 0]
        folder_list.sort(reverse=True, key=lambda date: datetime.strptime(date, "%Y-%m-%d"))

        # check Reorganizing
        cmd = f'cat {settings["LOGPATH"]}/{folder_list[0]}/logFile.log | grep `date "+%Y-%m-%dT%H:%M:"` | grep --color=never Reorganizing'
        isWarning = _find_reorganizing_log_lines(ssh, cmd)
        if isWarning:
            output[area]['Reorganizing'] = '\n'.join(isWarning)

        # check ERROR
        cmd = f'cat {settings["LOGPATH"]}/{folder_list[0]}/logFile.log | grep `date "+%Y-%m-%dT%H:%M:"` | grep -E "Error|ERROR" | wc -l'
        error_cnt = _find_error_log_lines(ssh, cmd)
        if error_cnt > 0:
            output[area]['ERROR'] = f'FIND ERROR LOG LINES. COUNT = {error_cnt}'

        # check block height
        cmd = f'cat {settings["LOGPATH"]}/{folder_list[0]}/logFile.log | grep `date "+%Y-%m-%dT%H:%M:"` | grep --color=never "Processed Block"'
        warning = _find_warning_block_height_log_lines(ssh, cmd)
        if warning is not None:
            output[area]['BLOCK HEIGHT'] = 'CHECK BLOCK HEIGHT...'
            output[area]['LOG LINES'] = warning

        # check block sync
        cmd = f'cat {settings["LOGPATH"]}/{folder_list[0]}/logFile.log | grep `date "+%Y-%m-%dT%H:%M:"` | grep "\[FATAL\] DBBatch - Modified the block" | wc -l'
        fatal_cnt = _find_error_log_lines(ssh, cmd)
        if fatal_cnt > 0:
            output[area]['FATAL'] = f'Modified the block. COUNT = {fatal_cnt}'

        ssh.sendline(f'unset HISTFILE')
        ssh.logout()
        ssh.close()
    except Exception as e:
        if logger is not None:
            logger.exception(e)
        if 'ssh' in globals() and ssh is not None:
            ssh.logout()
            ssh.close()


def wordpress_watchdog(config, logger):
    logger.info('wordpress_watchdog start')
    options = Options()
    options.add_argument('headless')
    options.add_argument('window-size=1920x1080')
    options.add_argument("disable-gpu")
    options.add_argument("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36")

    driver = webdriver.Chrome(f'{config.WORK_DIR}/{os_check()}_chromedriver', options=options)

    driver.get('about:blank')
    driver.execute_script("Object.defineProperty(navigator, 'plugins', {get: function() {return[1, 2, 3, 4, 5];},});")

    driver.implicitly_wait(3)

    driver.get('https://hycon.io')
    time.sleep(2)

    try:
        driver.find_element_by_id('user_login').send_keys(config.WORDPRESS_ADMIN_ID)
        driver.find_element_by_id('user_pass').send_keys(config.WORDPRESS_ADMIN_PW)
        driver.find_element_by_id('wp-submit').click()
        time.sleep(2)

        driver.get('https://hycon.io/wp-admin/options-permalink.php')
        driver.find_element_by_xpath('//input[@value="/archives/%post_id%"]').click()
        driver.find_element_by_xpath('//input[@id="submit"]').click()
        time.sleep(2)

        driver.find_element_by_xpath('//input[@value="/%postname%/"]').click()
        driver.find_element_by_xpath('//input[@id="submit"]').click()
        time.sleep(2)

        driver.quit()
    except NoSuchElementException as e:
        logger.info('hycon.io is Normal.')
    finally:
        driver.quit()





