# hycon_server_status_check_bot

AWS server status check and ERROR message arlam (telegram)

- function
> Hycon node server disk check.
>> Notify me if the usage of disk space used by the primary disk space. (90%)
>
> Hycon node logs analysis.
>> Reorganization count greater than 50
>>
>> ERROR count greater than 0
>>
>> The previous height is greater than the current height.


- required
> python3 v3.6+

- install 
> pip install -r requirements.txt

- run
> ./run 
>
> python3 [package_path]/run

