#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
from urllib import parse
from threading import Thread
from datetime import datetime
from pytz import timezone

from processors import Worker, WorkerProcess, Message
import common


class ProducerProcess(Worker):
    """작업을 배당하기 위한 선행으로 SSH 접속하여 정보를 읽어오는 역할"""
    def __init__(self, core):
        super().__init__()
        self.core = core
        self.default_polling_interval = self.config.DEFAULT_INTERVAL
        self.logger.info(f'{self.__class__.__name__} initialize call..')

    def polling(self):
        if self.core.analysis is not None:
            threads = []
            disk_outputs = {}
            log_outputs = {}
            if len(disk_outputs) > 0:
                disk_outputs.clear()

            if len(log_outputs) > 0:
                log_outputs.clear()

            # 지정시간에만 동작하도록 시간과 분을 체크
            if datetime.now(timezone('Asia/Seoul')).strftime('%H:%M') == self.config.DISK_CHECK_TIME:
                self.logger.info('GET DISK SPACE INFORMATION START...')
                for area, ipaddr in self.config.server_list.items():
                    thread = Thread(target=common.get_disk_usage_warning, args=(ipaddr, disk_outputs, self.logger))
                    threads.append(thread)
                    thread.start()

                for thread in threads:
                    thread.join()
                self.logger.info('GET DISK SPACE INFORMATION END...')
                self.core.analysis.add(Message('DISK', obj=disk_outputs))
                pass

            # 기본 인터벌 시간에 따라 로그 가져오기를 동작
            self.logger.info('GET LOGS INFORMATION START...')
            for area, ipaddr in self.config.server_list.items():
                thread = Thread(target=common.get_log_by_keyword, args=(self.config.server_logs, area, log_outputs, self.logger))
                threads.append(thread)
                thread.start()

            for thread in threads:
                thread.join()
            self.logger.info('GET LOGS INFORMATION END...')
            self.core.analysis.add(Message('LOG', obj=log_outputs))

        # 매일 12시 30분에 php 워드 프레스 사이트 확인
        if datetime.now(timezone('Asia/Seoul')).strftime('%H:%M') == '12:30':
            common.wordpress_watchdog(self.config, self.logger)


class AnalysisProcess(WorkerProcess):
    """전달 받은 메세지를 분류 하는 역할"""
    def __init__(self, core):
        super().__init__()
        self.core = core
        self.default_polling_interval = self.config.DEFAULT_INTERVAL
        self.logger.info(f'{self.__class__.__name__} initialize call...')

    def on_action(self, job_type, information):
        self.logger.info(f'{job_type} : {information}')
        for k, v in information.items():
            if v != {}:
                for in_k, in_v in v.items():
                    text = f"{k}\n{in_v}"
                    self.send_message(text)

    def send_message(self, text):
        self.logger.info(text)
        os.system(f'curl -X GET "{self.config.TELEGRAM_API_URL}{parse.quote(text)}"')

